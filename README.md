﻿# gitlab-runner

## What is it

Docker-in-Docker Gitlab runners setup.

## Configuration

All configuration can be set in .env file

| Environment               | Description                           | e.g.                                 |
| ------------------------- | ------------------------------------- | ------------------------------------ |
| RUNNER_NAME               | Runner name (default: "4a7fb667de6a") | loodos-gitlab-runner-linux-docker-64 |
| RUNNER_SERVER_URL         | Gitlab server URL                     | https://gitlab.com/                  |
| RUNNER_REGISTRATION_TOKEN | Runner's registration token           | \<secret>                            |
| RUNNER_TAG_LIST           | Tag list                              | loodos,linux,docker,123.12.12.123    |
| DOCKER_IMAGE              | Default docker image to be used       | docker:stable-dind                   |

## Build & Run

### Register Runner

`docker-compose -f docker-compose-register.yml up -d`

After succesfully registration `config` folder and `config\config.toml` file created. Then you can run compose down:

`docker-compose -f docker-compose-register.yml down`

### Run gitlab-runner:

`docker-compose up -d`

## License

This library is release under Apache 2.0 License.

Copyright (c) 2018 [Loodos](https://loodos.com)

See [LICENSE](https://gitlab.com/loodos/lds-library/lds-gitlab-runner/blob/master/LICENSE)

## Acknowledgements

Thanks for providing free open source licensing

- https://docs.gitlab.com/runner/install/docker.html
- https://docs.gitlab.com/runner/register/index.html
- https://medium.com/@tonywooster/docker-in-docker-in-gitlab-runners-220caeb708ca